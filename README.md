# POS Reports (Enterprise)

This addons is a Point of Sale extra addons, that allows user to print:

- X-Report.

- Z-Report.

- Sales Summary report.

- Product Summary Report.

- Order Summary Report.

- Payment Summary Report.

- Session Audit Report.

- Inventory Audit Report.


## Languages

This addon has support the following languages:

- Spanish.

- Spanish (Colombia).

- Spanish (Venezuela).


## License

Please see [LICENSE](LICENSE) file for more License information.


## Reference

- [POS Reports (Enterprise) at Odoo Apps Store](https://apps.odoo.com/apps/modules/15.0/aspl_pos_report_ee/).

- [Acespritech Solutions Pvt. Ltd. : Odoo ERP, Website & Mobile App Development Company](https://acespritech.com/).

