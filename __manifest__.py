#################################################################################
# Author      : Acespritech Solutions Pvt. Ltd. (<www.acespritech.com>)
# Copyright(c): 2012-Present Acespritech Solutions Pvt. Ltd.
# All Rights Reserved.
#
# This program is copyright property of the author mentioned above.
# You can`t redistribute it and/or modify it.
#
#################################################################################
{
    'name': 'POS Reports (Enterprise)',
    'category': 'Point of Sale',
    'summary': """Allows user to print X-Report, Z-Report, Sales Summary report,
            Product Summary Report,Order Summary Report,Payment Summary Report and 
            Session & Inventory Audit Report.""",
    'description': """
Allows user to print X-Report, Z-Report, Sales Summary report,
            Product Summary Report,Order Summary Report,Payment Summary Report
            and Session & Inventory Audit Report.
""",
    'author': "Acespritech Solutions Pvt. Ltd.",
    'website': "http://www.acespritech.com",
    'price': 80.00,
    'currency': 'EUR',
    'version': '1.0',
    'license':'LGPL-3',
    'depends': ['base', 'point_of_sale'],
    'images': [
        'static/description/icon.png',
        'static/description/main_screenshot.png',
    ],
    'data': [
        'security/ir.model.access.csv',
        'views/pos_config_view.xml',
        'report/reports.xml',
        'report/pos_sales_report_pdf_template.xml',
        'report/front_sales_report_pdf_template.xml',
        'report/front_inventory_session_pdf_report_template.xml',
        'report/front_inventory_location_pdf_report_template.xml',
        'report/sales_details_template.xml',
        'report/sales_details_pdf_template.xml',
        'report/customer_sale_and_return_report_template.xml',
        'report/customer_sale_and_return_report.xml',
        'report/pos_sales_report_template.xml',
        'report/front_sales_thermal_report_template.xml',
        'wizard/wizard_pos_sale_report_view.xml',
        'wizard/customer_sale_and_return_wizard_view.xml',
        'wizard/employee_hourly_sale_wizard_view.xml',
        'wizard/wizard_sales_details_view.xml',
        'wizard/wizard_pos_x_report.xml',
    ],
    'assets': {
        'point_of_sale.assets': [
           'aspl_pos_report_ee/static/src/css/pos.css',
           'aspl_pos_report_ee/static/src/js/models.js',
           'aspl_pos_report_ee/static/src/js/Printer.js',
           'aspl_pos_report_ee/static/src/js/Popups/AuditReportPopup.js',
           'aspl_pos_report_ee/static/src/js/Popups/OrderSummaryPopup.js',
           'aspl_pos_report_ee/static/src/js/Popups/PaymentSummaryPopup.js',
           'aspl_pos_report_ee/static/src/js/Popups/ProductSummaryPopup.js',
           'aspl_pos_report_ee/static/src/js/Screens/ProductScreen/ControlButtons/AuditReportButton.js',
           'aspl_pos_report_ee/static/src/js/Screens/ProductScreen/ControlButtons/OrderSummaryButton.js',
           'aspl_pos_report_ee/static/src/js/Screens/ProductScreen/ControlButtons/PaymentSummaryButton.js',
           'aspl_pos_report_ee/static/src/js/Screens/ProductScreen/ControlButtons/ProductSummaryButton.js',
           'aspl_pos_report_ee/static/src/js/Screens/ReceiptScreen/OrderSummaryReceipt.js',
           'aspl_pos_report_ee/static/src/js/Screens/ReceiptScreen/PaymentSummaryReceipt.js',
           'aspl_pos_report_ee/static/src/js/Screens/ReceiptScreen/ProductSummaryReceipt.js',
           'aspl_pos_report_ee/static/src/js/Screens/ReceiptScreen/ReceiptScreen.js',
        ],
        'web.assets_qweb': [
            'aspl_pos_report_ee/static/src/xml/**/*'
        ],
    },
    'installable': True,
    'auto_install': False,
}

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
