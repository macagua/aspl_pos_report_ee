#################################################################################
# Author      : Acespritech Solutions Pvt. Ltd. (<www.acespritech.com>)
# Copyright(c): 2012-Present Acespritech Solutions Pvt. Ltd.
# All Rights Reserved.
#
# This program is copyright property of the author mentioned above.
# You can`t redistribute it and/or modify it.
#
#################################################################################

import logging
from datetime import datetime

import pytz
from odoo import models
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT
from pytz import timezone

_logger = logging.getLogger(__name__)


class PosSession(models.Model):
    _inherit = "pos.session"

    def get_pos_name(self):
        if self and self.config_id:
            return self.config_id.name

    def get_inventory_details(self):
        product_product = self.env['product.product']
        stock_location = self.config_id.picking_type_id.default_location_src_id
        inventory_records = []
        final_list = []
        product_details = []
        if self and self.id:
            for order in self.order_ids:
                for line in order.lines:
                    product_details.append({
                        'id': line.product_id.id,
                        'qty': line.qty,
                    })
        custom_list = []
        for each_prod in product_details:
            if each_prod.get('id') not in [x.get('id') for x in custom_list]:
                custom_list.append(each_prod)
            else:
                for each in custom_list:
                    if each.get('id') == each_prod.get('id'):
                        each.update(
                            {'qty': each.get('qty') + each_prod.get('qty')})
        for each in custom_list:
            product_id = product_product.browse(each.get('id'))
            if product_id:
                inventory_records.append({
                    'product_id': [product_id.id, product_id.name],
                    'category_id': [product_id.id, product_id.categ_id.name],
                    'used_qty': each.get('qty'),
                    'quantity': product_id.with_context(
                        {'location': stock_location.id, 'compute_child': False}).qty_available,
                    'uom_name': product_id.uom_id.name or ''
                })
            if inventory_records:
                temp_list = []
                temp_obj = []
                for each in inventory_records:
                    if each.get('product_id')[0] not in temp_list:
                        temp_list.append(each.get('product_id')[0])
                        temp_obj.append(each)
                    else:
                        for rec in temp_obj:
                            if rec.get('product_id')[0] == each.get('product_id')[0]:
                                qty = rec.get('quantity') + \
                                    each.get('quantity')
                                rec.update({'quantity': qty})
                final_list = sorted(temp_obj, key=lambda k: k['quantity'])
        return final_list or []

    def get_total_closing(self):
        return self.cash_register_balance_end_real

    def get_total_tax(self):
        if self:
            self.env.cr.execute("""SELECT COALESCE(SUM(amount_tax), 0.0) AS total 
                                    FROM pos_order WHERE state != 'draft' 
                                    AND session_id = %s""" % self.id)
            return self.env.cr.dictfetchall()[0]['total']
        return 0.0

    def get_vat_tax(self):
        taxes_info = []
        if self:
            tax_list = [tax.id for order in self.order_ids for line in
                        order.lines.filtered(lambda line: line.tax_ids_after_fiscal_position) for
                        tax in
                        line.tax_ids_after_fiscal_position]
            tax_list = list(set(tax_list))
            for tax in self.env['account.tax'].browse(tax_list):
                total_tax = 0.00
                net_total = 0.00
                for line in self.env['pos.order.line'].search(
                    [('order_id', 'in', [order.id for order in self.order_ids])]).filtered(
                        lambda line: tax in line.tax_ids_after_fiscal_position):
                    total_tax += line.price_subtotal * tax.amount / 100
                    net_total += line.price_subtotal
                taxes_info.append({
                    'tax_name': tax.name,
                    'tax_total': total_tax,
                    'tax_per': tax.amount,
                    'net_total': net_total,
                    'gross_tax': total_tax + net_total
                })
        return taxes_info

    def get_session_date_time(self, flag):
        date_time_dict = {'start_date': '', 'end_date': '', 'time': ''}
        local = pytz.timezone(self._context.get('tz', 'utc') or 'utc')

        start_date = pytz.utc.localize(
            datetime.strptime(str(self.start_at), DEFAULT_SERVER_DATETIME_FORMAT))
        if self.stop_at:
            stop_date = pytz.utc.localize(
                datetime.strptime(str(self.stop_at), DEFAULT_SERVER_DATETIME_FORMAT))
            converted_stop_date = datetime.strftime(stop_date.astimezone(local),
                                                    DEFAULT_SERVER_DATETIME_FORMAT)
            date_time_dict.update({'end_date': converted_stop_date})
        final_converted_date = datetime.strftime(start_date.astimezone(local),
                                                 DEFAULT_SERVER_DATETIME_FORMAT)
        final_converted_time = datetime.strptime(final_converted_date,
                                                 DEFAULT_SERVER_DATETIME_FORMAT)
        date_time_dict.update({'time': final_converted_time.strftime('%I:%M:%S %p'),
                               'start_date': final_converted_time.date() if flag
                               else final_converted_time})
        return date_time_dict

    def get_current_date_time(self):
        user_tz = self.env.user.tz or pytz.utc
        return {'date': datetime.now(timezone(user_tz)).date(),
                'time': datetime.now(timezone(user_tz)).strftime('%I:%M:%S %p')}

    def get_total_sales(self):
        self.env.cr.execute("""SELECT
                                COALESCE(SUM(pol.price_unit * pol.qty), 0.0) AS total 
                                FROM pos_order AS po
                                LEFT JOIN pos_order_line AS pol ON pol.order_id = po.id 
                                WHERE po.amount_total > 0 
                                AND po.session_id = %s
                                AND po.state NOT IN ('draft', 'cancel')
                                """ % self.id)
        return self.env.cr.dictfetchall()[0]['total']

    def get_total_return_sales(self):
        self.env.cr.execute("""SELECT
                                COALESCE(SUM(amount_total), 0.0) AS total 
                                FROM pos_order
                                WHERE amount_total < 0
                                AND session_id = %s
                                AND state != 'draft'
                                """ % self.id)
        return abs(self.env.cr.dictfetchall()[0]['total'])

    def get_gross_total(self):
        gross_total = 0.0
        company_id = self.env.user.company_id.id
        domain = [
            ('state', '!=', 'draft'),
            ('company_id', '=', company_id),
            ('session_id', '=', self.id),
        ]
        pos_order_obj = self.env['pos.order'].search(domain)
        for order in pos_order_obj:
            for line in order.lines:
                gross_total += line.price_subtotal -  (line.qty * line.product_id.standard_price)
        return gross_total

    # def get_gross_profit(self):
    #     gross_profit_total = 0.0
    #     company_id = self.env.user.company_id.id
    #     domain = [
    #         ('state', '!=', 'draft'),
    #         ('company_id', '=', company_id),
    #         ('session_id', '=', self.id),
    #     ]
    #     # if user_id:
    #     #     domain.append(('user_id', '=', user_id))
    #     pos_order_obj = self.env['pos.order'].search(domain)
    #     for order in pos_order_obj:
    #         for line in order.lines:
    #             gross_profit_total += ((line.price_subtotal - (line.qty * line.product_id.standard_price)) / line.price_subtotal)*100
    #     return gross_profit_total

    # def get_net_profit(self):
    #     net_profit_total = 0.0
    #     company_id = self.env.user.company_id.id
    #     domain = [
    #         ('state', '!=', 'draft'),
    #         ('company_id', '=', company_id),
    #         ('session_id', '=', self.id),
    #     ]
    #     pos_order_obj = self.env['pos.order'].search(domain)
    #     for order in pos_order_obj:
    #         for line in order.lines:
    #             discount = (((line.price_unit * line.qty) * line.discount) / 100)
    #             net_profit_total += ((line.price_subtotal_incl - ((line.qty * line.product_id.standard_price) + discount)) / line.price_subtotal_incl)*100
    #     return net_profit_total
    
    def get_product_cate_total_x(self):
        balance_end_real = 0.0
        if+ self.order_ids:
            for order in self.order_ids:
                for line in order.lines:
                    balance_end_real += (line.qty * line.price_unit)
        return balance_end_real

    # PAYMENT DATA FROM X AND Z REPORT
    def get_payments(self):
        if self:
            sql = """SELECT ppm.id, ppm.name AS pay_method, SUM(pp.amount) AS amount 
                        FROM pos_payment AS pp
                        LEFT JOIN pos_payment_method AS ppm on ppm.id = pp.payment_method_id 
                        WHERE session_id = %s
                        GROUP BY ppm.name, ppm.id""" % self.id
            self.env.cr.execute(sql)
            result = self.env.cr.dictfetchall()
            return result and result or []

    # DISCOUNT DATA FROM X AND Z REPORT
    def get_total_discount(self):
        sql = """SELECT 
                    COALESCE(SUM((((pol.price_unit * pol.qty) * pol.discount) / 100)), 0.0) AS total
                    FROM pos_order AS po
                    INNER JOIN pos_order_line AS pol ON pol.order_id = po.id
                    WHERE session_id = %s 
                    AND po.state != 'draft'
                    AND pol.discount > 0 
                    AND pol.price_subtotal > 0""" % self.id
        self.env.cr.execute(sql)
        return abs(self.env.cr.dictfetchall()[0]['total'])

    # PRODUCT CATEGORY DATA FROM X AND Z REPORT
    def get_product_category(self):
        sql = """SELECT pc.name AS category, SUM(pol.price_subtotal_incl) AS total 
                    FROM pos_order as po
                    LEFT JOIN pos_order_line AS pol on pol.order_id = po.id
                    INNER JOIN product_product AS pp on pp.id = pol.product_id
                    INNER JOIN product_template AS pt on pt.id = pp.product_tmpl_id
                    LEFT JOIN pos_category AS pc on pc.id = pt.pos_categ_id
                    WHERE po.session_id = %s
                    AND po.state != 'draft'
                    GROUP BY pc.name, pc.id 
                """ % self.id
        self.env.cr.execute(sql)
        category_data = self.env.cr.dictfetchall()
        return category_data if category_data else []

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
