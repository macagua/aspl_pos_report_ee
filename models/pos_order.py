#################################################################################
# Author      : Acespritech Solutions Pvt. Ltd. (<www.acespritech.com>)
# Copyright(c): 2012-Present Acespritech Solutions Pvt. Ltd.
# All Rights Reserved.
#
# This program is copyright property of the author mentioned above.
# You can`t redistribute it and/or modify it.
#
#################################################################################

from datetime import datetime, time, timedelta
from operator import itemgetter

import pytz
from odoo import api, fields, models
from pytz import timezone, utc


def start_end_date_global(start, end, tz):
    tz = pytz.timezone(tz) or 'UTC'
    current_time = datetime.now(tz)
    hour_tz = int(str(current_time)[-5:][:2])
    min_tz = int(str(current_time)[-5:][3:])
    sign = str(current_time)[-6][:1]
    sdate = start + " 00:00:00"
    edate = end + " 23:59:59"
    start_date = ''
    end_date = ''
    if sign == '-':
        start_date = (datetime.strptime(sdate, '%Y-%m-%d %H:%M:%S') + timedelta(
            hours=hour_tz, minutes=min_tz)).strftime("%Y-%m-%d %H:%M:%S")
        end_date = (datetime.strptime(edate, '%Y-%m-%d %H:%M:%S') + timedelta(
            hours=hour_tz, minutes=min_tz)).strftime("%Y-%m-%d %H:%M:%S")
    if sign == '+':
        start_date = (datetime.strptime(sdate, '%Y-%m-%d %H:%M:%S') - timedelta(
            hours=hour_tz, minutes=min_tz)).strftime("%Y-%m-%d %H:%M:%S")
        end_date = (datetime.strptime(edate, '%Y-%m-%d %H:%M:%S') - timedelta(
            hours=hour_tz, minutes=min_tz)).strftime("%Y-%m-%d %H:%M:%S")

    return start_date, end_date


class PosConfig(models.Model):
    _inherit = 'pos.config'

    # product summary report
    print_product_summary = fields.Boolean(string="Product Summary Report")
    no_of_product_receipt = fields.Integer(string="No.of Copy Receipt")
    product_current_month_date = fields.Boolean(string="Current Month Date")
    signature = fields.Boolean(string="Signature")

    # order summary report
    enable_order_summary = fields.Boolean(string='Enable Order Summary')
    no_of_order_receipt = fields.Integer(string="No.of Order Receipt")
    order_current_month_date = fields.Boolean(string="Order Current Month Date")
    order_signature = fields.Boolean(string="Order Signature")

    # payment summary report
    payment_summary = fields.Boolean(string="Payment Summary")
    payment_current_month_date = fields.Boolean(string="Payment Current Month Date")

    # audit report
    print_audit_report = fields.Boolean("Print Audit Report")


class PosOrder(models.Model):
    _inherit = 'pos.order'

    @api.model
    def load_inventory_setting(self):
        settings_inventory = self.env['res.company'].sudo().search_read(
            domain=[('id', '=', self.env.user.company_id.id)],
            fields=['point_of_sale_update_stock_quantities'])
        if settings_inventory:
            return settings_inventory
        return False

    def summery_filter_orders_by_session(self, session_id, state=False):
        sql = ''
        if state:
            sql = """ SELECT id FROM pos_order WHERE session_id=%s AND state='%s';""" % (session_id, state)
        else: 
            sql = """ SELECT id FROM pos_order WHERE session_id=%s;""" % (session_id)
        self._cr.execute(sql)
        sql_result = (x[0] for x in self.env.cr.fetchall())
        order_ids = self.env['pos.order'].browse(sql_result)
        return order_ids

    def summery_filter_orders_by_time(self, start_date, end_date, state=False):
        local = pytz.timezone(self.env.user.tz)
        start_date = start_date + " 00:00:00"
        start_date_time = datetime.strptime(start_date, "%Y-%m-%d %H:%M:%S")
        start_local_dt = local.localize(start_date_time, is_dst=None)
        start_utc_dt = start_local_dt.astimezone(pytz.utc)
        string_utc_date_time = start_utc_dt.strftime('%Y-%m-%d %H:%M:%S')

        end_date = end_date + " 23:59:59"
        end_date_time = datetime.strptime(end_date, "%Y-%m-%d %H:%M:%S")
        end_local_dt = local.localize(end_date_time, is_dst=None)
        end_utc_dt = end_local_dt.astimezone(pytz.utc)
        string_end_utc_date_time = end_utc_dt.strftime('%Y-%m-%d %H:%M:%S')
        sql = ''
        if state:
            sql = """ SELECT id FROM pos_order WHERE state='%s' AND date_order BETWEEN '%s' AND '%s' """ % (state, string_utc_date_time, string_end_utc_date_time)
        else:
            sql = """ SELECT id FROM pos_order WHERE date_order BETWEEN '%s' AND '%s' """ % (string_utc_date_time, string_end_utc_date_time)
        self._cr.execute(sql)
        sql_result = (x[0] for x in self.env.cr.fetchall())
        order_ids = self.env['pos.order'].browse(sql_result)
        return order_ids


    @api.model
    def product_summary_report(self, val):
        product_summary_dict = {}
        category_summary_dict = {}
        payment_summary_dict = {}
        location_summary_dict = {}
        if val:
            if val.get('session_id'):
                order_detail = self.summery_filter_orders_by_session(val.get('session_id'))
            else:
                order_detail = self.summery_filter_orders_by_time(val.get('start_date'), val.get('end_date'))

            if 'product_summary' in val.get('summary') and order_detail or len(val.get('summary')) == 0 and order_detail:
                for each_order_line in order_detail.mapped('lines'):
                    if each_order_line.product_id.name in product_summary_dict:
                        product_qty = product_summary_dict[each_order_line.product_id.name]
                        product_qty += each_order_line.qty
                    else:
                        product_qty = each_order_line.qty
                    product_summary_dict[each_order_line.product_id.name] = product_qty

            if 'category_summary' in val.get('summary') and order_detail or len(val.get('summary')) == 0 and order_detail:
                for each_order_line in order_detail.mapped('lines'):
                    category_name = each_order_line.product_id.pos_categ_id.name
                    if category_name in category_summary_dict:
                        category_qty = category_summary_dict[category_name]
                        category_qty += each_order_line.qty
                    else:
                        category_qty = each_order_line.qty
                        category_summary_dict[category_name] = category_qty
                if False in category_summary_dict:
                    category_summary_dict['Others'] = category_summary_dict.pop(False)

            if 'payment_summary' in val.get('summary') and order_detail or len(val.get('summary')) == 0 and order_detail:
                for payment_line in order_detail.mapped('payment_ids'):
                    if payment_line.payment_method_id.name in payment_summary_dict:
                        payment = payment_summary_dict[payment_line.payment_method_id.name]
                        payment += payment_line.amount
                    else:
                        payment = payment_line.amount
                    payment_summary_dict[payment_line.payment_method_id.name] = float(
                        format(payment, '2f'))

            if 'location_summary' in val.get('summary') or len(val.get('summary')) == 0:
                session_list = []
                stock_picking_data = False
                for each_order in order_detail:
                    if each_order.session_id.id not in session_list:
                        session_list.append(each_order.session_id.id)

                if session_list:
                    stock_picking_data = self.env['stock.picking'].sudo().search(
                        [('pos_session_id', 'in', session_list)])

                if stock_picking_data:
                    for each_stock in stock_picking_data:
                        location_summary_dict[each_stock.location_id.name] = {}
                    for each_stock in stock_picking_data:
                        for each_stock_line in each_stock.move_ids_without_package:
                            if each_stock_line.product_id.name in \
                                    location_summary_dict[each_stock.location_id.name]:
                                location_qty = location_summary_dict[each_stock.location_id.name][
                                    each_stock_line.product_id.name]
                                location_qty += each_stock_line.quantity_done
                            else:
                                location_qty = each_stock_line.quantity_done
                            location_summary_dict[each_stock.location_id.name][
                                each_stock_line.product_id.name] = location_qty

        return {
            'ProductSummary': product_summary_dict,
            'CategorySummary': category_summary_dict,
            'PaymentSummary': payment_summary_dict,
            'LocationSummary': location_summary_dict,
        }

    @api.model
    def order_summary_report(self, val):
        order_list = {}
        order_list_sorted = []
        category_list = {}
        payment_list = {}
        if val and val['state'] == '':
            orders = False
            if val.get('session_id'):
                orders = self.summery_filter_orders_by_session(val.get('session_id'))
            else:
                orders = self.summery_filter_orders_by_time(val.get('start_date'), val.get('end_date'))
            
            if orders and 'order_summary_report' in val['summary'] or len(val['summary']) == 0:
                for each_order in orders:
                    order_list[each_order.state] = []
                for each_order in orders:
                    local = pytz.timezone(self.env.user.tz) or 'UTC'
                    date_order = each_order.date_order.astimezone(local)
                    if each_order.state in order_list:
                        order_list[each_order.state].append({
                            'order_ref': each_order.name,
                            'order_date': date_order,
                            'total': float(format(each_order.amount_total, '.2f'))
                        })
                    else:
                        order_list.update({
                            each_order.state.append({
                                'order_ref': each_order.name,
                                'order_date': date_order,
                                'total': float(format(each_order.amount_total, '.2f'))
                            })
                        })
            if orders and  'category_summary_report' in val['summary'] or len(val['summary']) == 0:
                count = 0.00
                amount = 0.00
                for each_order in orders:
                    category_list[each_order.state] = {}
                for each_order in orders:
                    for order_line in each_order.lines:
                        if each_order.state == 'paid':
                            if order_line.product_id.pos_categ_id.name in category_list[each_order.state]:
                                count = category_list[each_order.state][order_line.product_id.pos_categ_id.name][0]
                                amount = category_list[each_order.state][order_line.product_id.pos_categ_id.name][1]
                                count += order_line.qty
                                amount += order_line.price_subtotal_incl
                            else:
                                count = order_line.qty
                                amount = order_line.price_subtotal_incl
                        if each_order.state == 'done':
                            if order_line.product_id.pos_categ_id.name in category_list[each_order.state]:
                                count = category_list[each_order.state][order_line.product_id.pos_categ_id.name][0]
                                amount = category_list[each_order.state][order_line.product_id.pos_categ_id.name][1]
                                count += order_line.qty
                                amount += order_line.price_subtotal_incl
                            else:
                                count = order_line.qty
                                amount = order_line.price_subtotal_incl
                        if each_order.state == 'invoiced':
                            if order_line.product_id.pos_categ_id.name in category_list[each_order.state]:
                                count = category_list[each_order.state][order_line.product_id.pos_categ_id.name][0]
                                amount = category_list[each_order.state][order_line.product_id.pos_categ_id.name][1]
                                count += order_line.qty
                                amount += order_line.price_subtotal_incl
                            else:
                                count = order_line.qty
                                amount = order_line.price_subtotal_incl
                        category_list[each_order.state].update(
                            {order_line.product_id.pos_categ_id.name: [count, amount]})
                    if False in category_list[each_order.state]:
                        category_list[each_order.state]['others'] = category_list[
                            each_order.state].pop(False)

            if orders and  'payment_summary_report' in val['summary'] or len(val['summary']) == 0:
                count = 0
                for each_order in orders:
                    payment_list[each_order.state] = {}
                for each_order in orders:
                    for payment_line in each_order.payment_ids:
                        if each_order.state == 'paid':
                            if payment_line.payment_method_id.name in \
                                    payment_list[each_order.state]:
                                count = payment_list[each_order.state][
                                    payment_line.payment_method_id.name]
                                count += payment_line.amount
                            else:
                                count = payment_line.amount
                        if each_order.state == 'done':
                            if payment_line.payment_method_id.name in \
                                    payment_list[each_order.state]:
                                count = payment_list[each_order.state][
                                    payment_line.payment_method_id.name]
                                count += payment_line.amount
                            else:
                                count = payment_line.amount
                        if each_order.state == 'invoiced':
                            if payment_line.payment_method_id.name in \
                                    payment_list[each_order.state]:
                                count = payment_list[each_order.state][
                                    payment_line.payment_method_id.name]
                                count += payment_line.amount
                            else:
                                count = payment_line.amount
                        payment_list[each_order.state].update(
                            {payment_line.payment_method_id.name: float(format(count, '.2f'))})
            return {'orderReport': order_list, 'categoryReport': category_list,
                    'paymentReport': payment_list,
                    'State': val['state']}
        else:
            order_list = []
            if val.get('session_id'):
                orders = self.summery_filter_orders_by_session(val.get('session_id'), val.get('state'))
            else:
                orders = self.summery_filter_orders_by_time(val.get('start_date'), val.get('end_date'), val.get('state'))
            if orders and 'order_summary_report' in val['summary'] or len(val['summary']) == 0:
                for each_order in orders:
                    local = pytz.timezone(self.env.user.tz) or 'UTC'
                    date_order = each_order.date_order.astimezone(local)
                    order_list.append({
                        'order_ref': each_order.name,
                        'order_date': date_order,
                        'total': float(format(each_order.amount_total, '.2f'))
                    })
                order_list_sorted = sorted(order_list, key=itemgetter('order_ref'))
            if orders and 'category_summary_report' in val['summary'] or len(val['summary']) == 0:
                for each_order in orders:
                    for order_line in each_order.lines:
                        if order_line.product_id.pos_categ_id.name in category_list:
                            count = category_list[order_line.product_id.pos_categ_id.name][0]
                            amount = category_list[order_line.product_id.pos_categ_id.name][1]
                            count += order_line.qty
                            amount += order_line.price_subtotal_incl
                        else:
                            count = order_line.qty
                            amount = order_line.price_subtotal_incl
                        category_list.update(
                            {order_line.product_id.pos_categ_id.name: [count, amount]})
                if False in category_list:
                    category_list['others'] = category_list.pop(False)
            if orders and 'payment_summary_report' in val['summary'] or len(val['summary']) == 0:
                for each_order in orders:
                    for payment_line in each_order.payment_ids:
                        if payment_line.payment_method_id.name in payment_list:
                            count = payment_list[payment_line.payment_method_id.name]
                            count += payment_line.amount
                        else:
                            count = payment_line.amount
                        payment_list.update(
                            {payment_line.payment_method_id.name: float(format(count, '.2f'))})
        return {
            'orderReport': order_list_sorted,
            'categoryReport': category_list,
            'paymentReport': payment_list,
            'State': val['state']
        }

    @api.model
    def prepare_payment_summary_data(self, row_data, key):
        payment_details = {}
        summary_data = {}

        for each in row_data:
            if key == 'journals':
                payment_details.setdefault(each['month'], {})
                payment_details[each['month']].update({each['name']: each['amount']})
                summary_data.setdefault(each['name'], 0.0)
                summary_data.update({each['name']: summary_data[each['name']] + each['amount']})
            else:
                payment_details.setdefault(each['login'], {})
                payment_details[each['login']].setdefault(each['month'], {each['name']: 0})
                payment_details[each['login']][each['month']].update({each['name']: each['amount']})

        return [payment_details, summary_data]

    @api.model
    def payment_summary_report(self, vals):
        sql = False
        final_data_dict = dict.fromkeys(
            ['journal_details', 'salesmen_details', 'summary_data'], {})
        current_time_zone = self.env.user.tz or 'UTC'
        if vals.get('session_id'):
            if vals.get('summary') == 'journals':
                sql = """ SELECT
                            REPLACE(CONCAT(to_char(to_timestamp(
                            EXTRACT(
                            month FROM pp.payment_date AT TIME ZONE 'UTC' AT TIME ZONE '%s')::text,
                             'MM'),'Month'),
                            '-',
                            EXTRACT(
                            year FROM pp.payment_date AT TIME ZONE 'UTC' AT TIME ZONE '%s')),
                            ' ', '') AS month,
                            ppm.name, ppm.id,
                            SUM(pp.amount) AS amount
                            FROM pos_payment AS pp
                            INNER JOIN pos_payment_method AS ppm ON ppm.id = pp.payment_method_id
                            WHERE session_id = %s
                            GROUP BY month, ppm.name, ppm.id
                            ORDER BY month ASC
                        """ % (current_time_zone, current_time_zone, vals.get('session_id'))
            if vals.get('summary') == 'sales_person':
                sql = """ SELECT
                            REPLACE(CONCAT(to_char(to_timestamp(
                            EXTRACT(
                            month FROM pp.payment_date AT TIME ZONE 'UTC' AT TIME ZONE '%s')::text, 
                            'MM'), 'Month'), 
                            '-',EXTRACT(year FROM pp.payment_date AT TIME ZONE 'UTC' 
                            AT TIME ZONE '%s')),
                            ' ', '') AS month,
                            rp.name AS login, ppm.name, SUM(pp.amount) AS amount
                            FROM
                            pos_order AS po
                            INNER JOIN res_users AS ru ON ru.id = po.user_id
                            INNER JOIN res_partner AS rp ON rp.id = ru.partner_id
                            INNER JOIN pos_payment AS pp ON pp.pos_order_id = po.id
                            INNER JOIN pos_payment_method AS ppm ON ppm.id = pp.payment_method_id
                            WHERE
                            po.session_id = %s
                            GROUP BY ppm.name, rp.name, month""" % (
                    current_time_zone, current_time_zone, vals.get('session_id'))
        else:
            tz = self.env.user.tz
            start_dt = datetime.utcnow()
            if tz:
                start_date = timezone(tz).localize(start_dt).astimezone(utc).date()
            else:
                start_date = datetime.today().date()
            
            end_dt = datetime.combine(start_date, time.max)
            
            if tz:
                end_dt = timezone(tz).localize(end_dt).astimezone(utc)
            fields.Datetime.to_string(start_dt)

            s_date, e_date = start_end_date_global(vals.get('start_date'), vals.get('end_date'), current_time_zone)

            if vals.get('summary') == 'journals':
                sql = """ SELECT
                            REPLACE(CONCAT(to_char(to_timestamp(
                            EXTRACT(
                            month FROM pp.payment_date AT TIME ZONE 'UTC' AT TIME ZONE '%s')::text,
                             'MM'),'Month'),
                            '-',
                            EXTRACT(
                            year FROM pp.payment_date AT TIME ZONE 'UTC' AT TIME ZONE '%s')),
                            ' ', '') AS month,
                            ppm.name, ppm.id,
                            SUM(pp.amount) AS amount
                            FROM pos_payment AS pp
                            INNER JOIN pos_payment_method AS ppm ON ppm.id = pp.payment_method_id
                            WHERE payment_date BETWEEN  '%s' AND '%s'
                            GROUP BY month, ppm.name, ppm.id
                            ORDER BY month ASC
                        """ % (current_time_zone, current_time_zone, s_date, e_date)

            if vals.get('summary') == 'sales_person':
                sql = """ SELECT
                            REPLACE(CONCAT(to_char(to_timestamp(
                            EXTRACT(
                            month FROM pp.payment_date AT TIME ZONE 'UTC' AT TIME ZONE '%s')::text, 
                            'MM'), 'Month'), 
                            '-',
                            EXTRACT(
                            year FROM pp.payment_date AT TIME ZONE 'UTC' AT TIME ZONE '%s')),
                            ' ', '') AS month,
                            rp.name AS login, ppm.name, SUM(pp.amount) AS amount
                            FROM
                            pos_order AS po
                            INNER JOIN res_users AS ru ON ru.id = po.user_id
                            INNER JOIN res_partner AS rp ON rp.id = ru.partner_id
                            INNER JOIN pos_payment AS pp ON pp.pos_order_id = po.id
                            INNER JOIN pos_payment_method AS ppm ON ppm.id = pp.payment_method_id
                            WHERE
                            po.date_order BETWEEN '%s' AND '%s'
                            GROUP BY ppm.name, rp.name, month""" % (
                    current_time_zone, current_time_zone, s_date, e_date)
        if sql:
            self._cr.execute(sql)
            sql_result = self._cr.dictfetchall()

            if sql_result:
                result = self.prepare_payment_summary_data(sql_result, vals.get('summary'))
                if vals.get('summary') == 'journals':
                    final_data_dict.update({'JournalData': result[0], 'TotalSummary': result[1]})
                    return final_data_dict
                else:
                    final_data_dict.update({'SalesMenData': result[0]})
                    return final_data_dict
            else:
                return final_data_dict
        else:
            return final_data_dict


class IrActionsReport(models.Model):
    _inherit = 'ir.actions.report'

    @api.model
    def get_html_report(self, id, report_name):
        report = self._get_report_from_name(report_name)
        document = report.render_qweb_html([id], data={})
        if document:
            return document
        return False

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4
