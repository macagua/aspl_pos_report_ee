odoo.define('aspl_pos_report_ee.ProductSummaryButton', function(require) {
    'use strict';

    const PosComponent = require('point_of_sale.PosComponent');
    const ProductScreen = require('point_of_sale.ProductScreen');
    const { useListener } = require('web.custom_hooks');
    const Registries = require('point_of_sale.Registries');
    var rpc = require('web.rpc');

    class ProductSummaryButton extends PosComponent {
        constructor() {
            super(...arguments);
            useListener('click', this.onClick);
        }
        async getProductReportData(val){
            return this.rpc({
                model: 'pos.order',
                method: 'product_summary_report',
                args: [val]
            });
        }
        async onClick() {
            const { confirmed, payload } = await this.showPopup('ProductSummaryPopup', {
                title: this.env._t('Product Summary'),
            });

            if (confirmed) {
                var self = this;
                var order = self.env.pos.get_order();
                var product_val = {};
                var reportValueList = [];

                const {StartDate, EndDate, CurrentSession, ProductSummary,
                        CategorySummary, LocationSummary, PaymentSummary, ProdNumberReceipt} = payload;

                ProductSummary   ? reportValueList.push('product_summary') : '';
                CategorySummary  ? reportValueList.push('category_summary') : '';
                LocationSummary  ? reportValueList.push('location_summary') : '';
                PaymentSummary   ? reportValueList.push('payment_summary') : '';

                if(CurrentSession){
                    Object.assign(product_val, {summary: reportValueList,
                                                  session_id:  this.env.pos.pos_session.id});
                    self.env.pos.print_date = false;
                } else {
                    Object.assign(product_val, {summary: reportValueList,
                                                  start_date: StartDate, end_date: EndDate});
                    self.env.pos.print_date = true;
                    self.env.pos.date_information = [ StartDate, EndDate ];
                }
                if(product_val && !$.isEmptyObject(product_val)) {
                    let ProductReportData = await this.getProductReportData(product_val);
                    const {ProductSummary, CategorySummary, PaymentSummary, LocationSummary} = ProductReportData;
                    if($.isEmptyObject(ProductSummary) && $.isEmptyObject(CategorySummary)
                        && $.isEmptyObject(PaymentSummary) && $.isEmptyObject(LocationSummary)){
                        alert("No records found!");
                        return;
                    }
                    if (this.env.pos.config.iface_print_via_proxy) {
                        const report = this.env.qweb.renderToString('ProductSummaryReceipt',{
                            props: {
                                'CompanyData': order.getOrderReceiptEnv().receipt,
                                'ProductSummaryData': !$.isEmptyObject(ProductSummary) ? ProductSummary : false,
                                'CategorySummaryData': !$.isEmptyObject(CategorySummary) ? CategorySummary : false,
                                'PaymentSummaryData': !$.isEmptyObject(PaymentSummary) ? PaymentSummary : false,
                                'LocationSummaryData': !$.isEmptyObject(LocationSummary) ? LocationSummary : false,
                            }, env : this.env
                        });
                        const rec = Array(Number(ProdNumberReceipt) || 1).fill(report)
                        const printResult = await this.env.pos.proxy.printer.print_receipt(rec);
                        if (!printResult.successful) {
                            await this.showPopup('ErrorPopup', {
                                title: printResult.message.title,
                                body: printResult.message.body,
                            });
                        }
                    } else{
                        self.showScreen('ReceiptScreen', {'check':'from_product_summary',
                            'receipt': order.getOrderReceiptEnv().receipt,
                            'product_details': !$.isEmptyObject(ProductSummary) ? ProductSummary : false,
                            'category_details': !$.isEmptyObject(CategorySummary) ? CategorySummary : false,
                            'payment_details': !$.isEmptyObject(PaymentSummary) ? PaymentSummary : false,
                            'location_details': !$.isEmptyObject(LocationSummary) ? LocationSummary : false,
                        });
                    }
                }
            }
        }
    }
    ProductSummaryButton.template = 'ProductSummaryButton';

    ProductScreen.addControlButton({
        component: ProductSummaryButton,
        condition: function() {
            return this.env.pos.config.print_product_summary;
        },
    });

    Registries.Component.add(ProductSummaryButton);

    return ProductSummaryButton;

});
