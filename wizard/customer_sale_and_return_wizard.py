# -*- coding: utf-8 -*-
#################################################################################
# Author      : Acespritech Solutions Pvt. Ltd. (<www.acespritech.com>)
# Copyright(c): 2012-Present Acespritech Solutions Pvt. Ltd.
# All Rights Reserved.
#
# This program is copyright property of the author mentioned above.
# You can`t redistribute it and/or modify it.
#
#################################################################################
from odoo import fields, models, _
from odoo.exceptions import ValidationError


class CustomerSaleReport(models.TransientModel):
    _name = 'customer.sales.report'
    _description = 'Customer Sale Report'

    start_date = fields.Date('Start Date')
    end_date = fields.Date('End Date')
    customer_ids = fields.Many2many('res.partner', 'partner_id', 'wizard_id', string="Customers")

    def customer_sale_report_print(self):
        if self.start_date and self.end_date:
            if self.start_date > self.end_date:
                raise ValidationError(_("End Date should be grater than Start Date."))
        if self.customer_ids:
            domain = [('partner_id', 'in', self.customer_ids.ids),
                      ('date_order', '>=', self.start_date),
                      ('date_order', '<=', self.end_date)]
            order_ids = self.env['pos.order'].search(domain)
            customer_ids = self.customer_ids.ids
        else:
            domain = [('date_order', '>=', self.start_date),
                      ('date_order', '<=', self.end_date)]
            order_ids = self.env['pos.order'].search(domain)
            customer_ids = order_ids.mapped("partner_id").mapped("id")
        datas = {
            'customer_ids': customer_ids,
            'order_ids': order_ids.ids,
            'start_date': self.start_date,
            'end_date': self.end_date,
            'model': 'dr.visit',
            'form': self.read()[0],
        }
        report_id = self.env.ref('aspl_pos_report_ee.customer_sale_and_return_report')
        return report_id.report_action(self, data=datas)

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
